from rest_framework import serializers, renderers
from backend_apps.users import models
from backend_apps.musics import serializers as musics_serializers


class SocialNetworkSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.SocialNetwork
        fields = '__all__'


class DescriptionsFilesSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.DescriptionsFiles
        fields = '__all__'


class DescriptionsSerializer(serializers.ModelSerializer):
    social_network = SocialNetworkSerializer()
    descriptions_files = DescriptionsFilesSerializer(many=True)

    class Meta:
        model = models.Descriptions
        fields = ("id", "user", "description", "social_network", "descriptions_files")


class AuthUserSerializer(serializers.Serializer):
    email = serializers.CharField()
    password = serializers.CharField()


class SingUpSerializer(serializers.ModelSerializer):
    genres = serializers.ListSerializer(child=serializers.UUIDField(), required=False)

    class Meta:
        model = models.User
        fields = (
            "email",
            "username",
            "genres",
            "gender",
            "birthday",
            "mailing",
            "password",
            "avatar",
        )


class LogoutUserSerializer(serializers.Serializer):
    token = serializers.CharField()


class ProfileSerializer(serializers.ModelSerializer):
    playlists = musics_serializers.PlaylistSerializer(many=True, read_only=True)
    user_songs = musics_serializers.MusicSerializer(many=True)
    descriptions = DescriptionsSerializer()

    class Meta:
        model = models.User
        fields = (
            "email",
            "username",
            "gender",
            "subscribers",
            "avatar",
            "user_songs",
            "playlists",
            "descriptions",
            "is_artist",
            "premium_user",
            "is_superuser",
            "is_verified",
        )

    def to_representation(self, instance):
        instance = super(ProfileSerializer, self).to_representation(instance)
        if not instance['is_artist']:
            instance.pop("user_songs", None)
            instance.pop("descriptions", None)
        return instance


class SelectUserSerializer(serializers.ModelSerializer):

    class Meta:
        model = models.User
        fields = ("id", "username", "avatar", "is_artist")
