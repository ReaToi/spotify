from rest_framework.views import APIView
from django_filters.rest_framework import DjangoFilterBackend
from backend_apps.users import models, serializers
from rest_framework import permissions, generics, views, response, status, filters
from rest_framework.authtoken.models import Token
from django.db.models import Q


# Create your views here.


class AuthUserView(generics.CreateAPIView):
    serializer_class = serializers.AuthUserSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user = models.User.objects.get(
                Q(email=serializer.validated_data['email']) |
                Q(username=serializer.validated_data['email'])
            )
            token, created = Token.objects.get_or_create(user=user)
            return response.Response(data={'token': f"{token}"})

        except models.User.DoesNotExist:
            return response.Response(
                data={'error': 'User not found'}, status=status.HTTP_404_NOT_FOUND
            )


class UserSingUpAPIView(generics.CreateAPIView):
    serializer_class = serializers.SingUpSerializer
    permission_classes = [permissions.AllowAny]

    def post(self, request, *args, **kwargs):
        serializer = self.serializer_class(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            genres = serializer.validated_data.pop('genres', None)
            user = models.User.objects.create_user(**serializer.validated_data)
            if genres:
                user.genres.set(genres)
            token, created = Token.objects.get_or_create(user=user)
            data = dict(self.serializer_class(user).data)
            return response.Response(
                data={**data, 'token': f"{token}"}
            )

        except Exception as e:
            return response.Response(
                data={
                    'error': 'User with this username or email exists',
                    "detail": e.__str__(),
                },
                status=status.HTTP_400_BAD_REQUEST,
            )


class LogoutAPIView(generics.CreateAPIView):
    serializer_class = serializers.LogoutUserSerializer

    def post(self, request, *args, **kwargs):
        user = request.user
        try:
            serializer = self.serializer_class(data=request.data)
            serializer.is_valid(raise_exception=True)
            Token.objects.get(
                user=user, key=serializer.validated_data["token"]
            ).delete()
            return response.Response(data={'result': "You logout successfully!"})

        except Token.DoesNotExist:
            return response.Response(
                data={"message": "Token does not exist."},
                status=status.HTTP_404_NOT_FOUND,
            )


class ProfileAPIView(generics.RetrieveAPIView):
    serializer_class = serializers.ProfileSerializer
    queryset = models.User.objects.all()
    lookup_field = 'id'


class SelectArtistsAPIView(generics.ListAPIView):
    queryset = models.User.objects.filter(is_artist=True)
    serializer_class = serializers.SelectUserSerializer


class MyProfileAPIView(views.APIView):
    serializer_class = serializers.ProfileSerializer

    def get(self, request, *args, **kwargs):
        user = models.User.objects.get(id=request.user.id)
        serializer = self.serializer_class(user)
        return response.Response(serializer.data, status=status.HTTP_200_OK)


class SearchArtistsAPIView(generics.ListAPIView):
    queryset = models.User.objects.all()
    serializer_class = serializers.ProfileSerializer
    filter_backends = [filters.SearchFilter, DjangoFilterBackend]
    filterset_fields = ["is_artist"]
    search_fields = ["username"]


    # def get_queryset(self):
    #     return models.User.objects.filter(username=self.request.query_params['username'], is_artist=True)

