from django.urls import path
from backend_apps.users import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("auth/", views.AuthUserView.as_view(), name="login"),
    path("sing_up/", views.UserSingUpAPIView.as_view(), name="sing-up"),
    path("logout/", views.LogoutAPIView.as_view(), name='logout'),
    path("profile/<uuid:id>/", views.ProfileAPIView.as_view(), name="profile"),
    path(
        "select-artists/", views.SelectArtistsAPIView.as_view(), name="select artists"
    ),
    path("my-profile/", views.MyProfileAPIView.as_view(), name="my profile"),
    path("search-user/", views.SearchArtistsAPIView.as_view(), name="search user"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
