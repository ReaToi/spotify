from django.contrib import admin
from backend_apps.users import models
from django.contrib.auth.admin import UserAdmin as BaseUserAdmin

# Register your models here


class DescriptionInLines(admin.StackedInline):
    model = models.Descriptions


# @admin.register(models.User)
# class UserAdmin(BaseUserAdmin):
#     list_display = ("username", "email", "subscribers", "is_active", "is_artist", "is_distributor")
#     list_display_links = ("username", "email", "subscribers")
#     search_fields = ("username", "email")
#     list_filter = ("is_active", "is_artist", "is_distributor")
#     inlines = (DescriptionInLines,)


class DescriptionFilesInLines(admin.StackedInline):
    model = models.DescriptionsFiles


@admin.register(models.Descriptions)
class Descriptions(admin.ModelAdmin):
    list_display = ('id', 'user')
    list_display_links = ('user',)
    inlines = (DescriptionFilesInLines,)


admin.site.register(models.SocialNetwork)
