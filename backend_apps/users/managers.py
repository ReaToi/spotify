from django.contrib.auth.models import BaseUserManager


class UserManager(BaseUserManager):

    def _create_user(self, password, email, username, **extra_fields):
        if not username:
            raise ValueError("Users must have a username.")

        user = self.model(username=username, email=email, **extra_fields)
        # user = self.model(username=email, email=username, **extra_fields)
        user.set_password(password)
        user.save(using=self._db)
        return user

    def create_user(self, password, email, username, **extra_fields):
        return self._create_user(password, username, email, **extra_fields)

    def create_superuser(self, username, email, password):
        return self._create_user(
            password,
            email,
            username,
            is_staff=True,
            is_superuser=True,
            is_distributor=True,
            premium_user=True,
        )
