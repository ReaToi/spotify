from django.contrib.auth.validators import UnicodeUsernameValidator
from django.db import models
from django.contrib.auth.models import AbstractBaseUser, PermissionsMixin
from backend_apps.users.constants import GENDER
from backend_apps.users.managers import UserManager
from backend_apps.common.models import BaseModel, Genres
from django.utils.translation import gettext_lazy as _


class User(AbstractBaseUser, BaseModel, PermissionsMixin):
    username_validator = UnicodeUsernameValidator()
    username = models.CharField(
        _("username"),
        max_length=150,
        unique=True,
        help_text=_(
            "Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only."
        ),
        validators=[username_validator],
        error_messages={"unique": _("A user with that username already exists.")},
    )
    gender = models.CharField(_("gender"), max_length=20, choices=GENDER)
    email = models.EmailField(_("email address"), unique=True)
    birthday = models.DateField(_("birthday"), blank=True, null=True)
    subscribers = models.PositiveIntegerField(default=0)
    avatar = models.ImageField(
        upload_to="users/avatars/", default="users/avatars/default_user.jpeg"
    )
    genres = models.ManyToManyField(Genres, blank=True)
    mailing = models.BooleanField(_("newsletter subscription"), default=True)
    is_active = models.BooleanField(_("active"), default=True)
    is_staff = models.BooleanField(_("staff status"), default=False)
    is_artist = models.BooleanField(_("is artist"), default=False)
    is_distributor = models.BooleanField(_("is distributor"), default=False)
    premium_user = models.BooleanField(_("is premium user"), default=False)
    is_verified = models.BooleanField(_("is verified user"), default=False)
    USERNAME_FIELD = "email"
    REQUIRED_FIELDS = ["username"]

    objects = UserManager()

    class Meta:
        verbose_name = _("user")
        verbose_name_plural = _("users")

    def __str__(self):
        return f"{self.username}, {self.email}"


class SocialNetwork(BaseModel):
    photo = models.FileField(_("photo"))
    title = models.CharField(_("title"), max_length=50)
    link = models.URLField()

    def __str__(self):
        return self.title


class Descriptions(BaseModel):
    user = models.OneToOneField(
        User,
        verbose_name=_("user"),
        on_delete=models.CASCADE,
        related_name="descriptions",
    )
    description = models.TextField(_("description"))
    social_network = models.ForeignKey(
        SocialNetwork,
        verbose_name=_("social network"),
        on_delete=models.CASCADE,
        related_name="social_network",
    )

    def __str__(self):
        return f"{self.user} --- {self.social_network}"


class DescriptionsFiles(BaseModel):
    descriptions = models.ForeignKey(
        Descriptions,
        on_delete=models.CASCADE,
        verbose_name=_("descriptions"),
        related_name="descriptions_files",
    )
    file = models.FileField(
        _("file"), upload_to="users/descriptions/", null=True, blank=True
    )

    def __str__(self):
        return f"{self.descriptions} --- {self.file}"


class Subscription(BaseModel):
    user = models.ForeignKey(
        User, verbose_name=_("user"), on_delete=models.CASCADE, related_name="user"
    )
    subscription = models.ForeignKey(
        User,
        verbose_name=_("subscription"),
        on_delete=models.CASCADE,
        related_name="subscription",
    )

    def __str__(self):
        return f"{self.user} --- {self.subscription}"


class PasswordResetToken(BaseModel):
    user = models.ForeignKey(User, on_delete=models.CASCADE)
    token = models.CharField(max_length=255)
    time = models.DateTimeField()

    def __str__(self):
        return f"{self.user}, {self.token}"
