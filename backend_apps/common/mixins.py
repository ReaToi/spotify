from backend_apps.users import models, serializers
from backend_apps.musics import models as music_models, serializers as music_serializers
from django.db.models import Q
from backend_apps.common import models as common_models, serializers as common_serializers


class GlobalSearchMixin:

    def global_querylist(self, querylist, search):
        querylist = (
            {
                "queryset": models.User.objects.filter(Q(username__icontains=search), is_artist=True),
                "serializer_class": serializers.SelectUserSerializer,
            },
            {
                "queryset": music_models.Album.objects.filter(
                    Q(title__icontains=search)
                ),
                "serializer_class": music_serializers.AlbumSerializer,
            },
            {
                "queryset": music_models.Playlist.objects.filter(
                    Q(title__icontains=search)
                ),
                "serializer_class": music_serializers.PlaylistSerializer,
            },
            {
                "queryset": music_models.Music.objects.filter(Q(title__icontains=search)),
                "serializer_class": music_serializers.MusicSerializer
            },
            {
                "queryset": models.User.objects.filter(Q(username__icontains=search), is_artist=False),
                "serializer_class": serializers.SelectUserSerializer,
            },
        )
        return querylist
