from rest_framework import generics, response
from backend_apps.common import models, serializers
from drf_multiple_model.views import ObjectMultipleModelAPIView
from .mixins import GlobalSearchMixin

# Create your views here.


class GenresAPIView(generics.ListAPIView):
    queryset = models.Genres.objects.all()
    serializer_class = serializers.GenresSerializer


class GlobalSearch(ObjectMultipleModelAPIView, GlobalSearchMixin):

    def get_querylist(self):
        querylist = ()
        search = self.request.query_params["q"]
        if search:
            return self.global_querylist(querylist, search)
        return querylist
