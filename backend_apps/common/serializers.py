from rest_framework import serializers
from backend_apps.users import models as user_models


class UserListSerializer(serializers.ModelSerializer):

    class Meta:
        model = user_models.User
        fields = ('id', 'username')


class GenresSerializer(serializers.ModelSerializer):

    class Meta:
        model = user_models.Genres
        fields = ('id', 'name')
