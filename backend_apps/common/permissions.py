from rest_framework import permissions


class IsArtistPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_artist or request.user.is_superuser)


class IsDistributorPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.is_distributor or request.user.is_superuser)


class PremiumPermission(permissions.BasePermission):

    def has_permission(self, request, view):
        return bool(request.user and request.user.premium_user or request.user.is_superuser)
