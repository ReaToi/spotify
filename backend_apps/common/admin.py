from django.contrib import admin
from backend_apps.users import models

# Register your models here.


@admin.register(models.User)
class UserAdmin(admin.ModelAdmin):
    list_display = ('id', 'username', 'email', "is_artist", "is_distributor")
    list_display_links = ('username', "email")
    list_filter = ("is_distributor", "is_artist")
    search_fields = ("username", "email")


@admin.register(models.Genres)
class GenresAdmin(admin.ModelAdmin):
    list_display = ('id', 'name')
    list_display_links = ('id', 'name')
