import uuid

from django.utils import timezone
from django.utils.translation import gettext_lazy as _
from django.db import models

# Create your models here.


class BaseModel(models.Model):
    id = models.UUIDField(
        primary_key=True,
        default=uuid.uuid4,
        editable=False,
        unique=True,
        verbose_name=_("ID"),
    )
    is_deleted = models.BooleanField(default=False, verbose_name=_("is deleted?"))
    date_joined = models.DateTimeField(_("date joined"), auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True, verbose_name=_("updated at"))

    class Meta:
        abstract = True


class Genres(BaseModel):
    name = models.CharField(max_length=50)

    def __str__(self):
        return self.name
