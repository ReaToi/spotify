from django.urls import path
from backend_apps.musics import views
from django.conf.urls.static import static
from django.conf import settings

urlpatterns = [
    path("my-favorites/", views.MyFavoriteMusicsAPIView.as_view(), name="my-favorites"),
    path("add-music/<uuid:album>/", views.MusicsAPIView.as_view(), name="add-music"),
    path(
        "my-created-playlists/",
        views.MyCreatedPlaylistsAPIView.as_view({'get': 'list', "post": "create"}),
        name="my-created-playlists",
    ),
    path(
        "my-created-playlists/<uuid:id>/",
        views.MyCreatedPlaylistsAPIView.as_view(
            {"get": "retrieve", "delete": "destroy", "patch": "partial_update"}
        ),
        name="my-created-playlists-detail",
    ),
    path(
        "delete-favorite-music/<uuid:id>/",
        views.MyFavoriteMusicsDeleteAPIView.as_view(),
        name="delete-favorite-music",
    ),
    path("albums/", views.AlbumsAPIView.as_view(), name="albums"),
    path("search-music/", views.SearchMusicAPIView.as_view(), name="search-music"),
    path("search-albums/", views.SearchAlbumsAPIView.as_view(), name="search-albums"),
    path("search-playlist/", views.SearchPlaylistAPIView.as_view(), name="search-playlist"),
    path("other-playlists/", views.OtherPlaylistAPIView.as_view(), name="other-playlists"),
    path("playlist/<uuid:id>/", views.DetailPlaylistAPIView.as_view(), name="detail-playlist"),
    path("album/<uuid:id>/", views.DetailAlbumAPIView.as_view(), name="detail-album"),
]

urlpatterns += static(settings.MEDIA_URL, document_root=settings.MEDIA_ROOT)
urlpatterns += static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
