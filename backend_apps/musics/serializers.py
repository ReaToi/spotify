from rest_framework import serializers
from backend_apps.musics import models
from backend_apps.common import serializers as common_serializers


class MusicSerializer(serializers.ModelSerializer):
    album_title = serializers.CharField(source='album.title', read_only=True)
    user = common_serializers.UserListSerializer(many=True, read_only=True)
    preview_image = serializers.FileField(source="album.preview_image", read_only=True)
    artists = serializers.ListSerializer(child=serializers.UUIDField(), write_only=True)

    class Meta:
        model = models.Music
        fields = (
            'id',
            'title',
            'duration',
            'auditions',
            'date_joined',
            'album_title',
            'preview_image',
            'user',
            "artists",
            "file",
            "album",
        )
        read_only_fields = ("auditions", "album")


class ListMusicsSerializer(serializers.ModelSerializer):
    music = MusicSerializer(many=True, read_only=True)
    list_music = serializers.UUIDField(write_only=True, required=False)

    class Meta:
        model = models.FavoriteMusics
        fields = ("id", "user", "music", "list_music")
        read_only_fields = ("user", "music")


class PlaylistSerializer(ListMusicsSerializer):
    username = serializers.CharField(source="user.username", read_only=True)
    # users = serializers.ListSerializer(child=serializers.UUIDField(), write_only=True)

    class Meta:
        model = models.Playlist
        fields = (
            "id",
            "user",
            "title",
            "description",
            "avatar",
            "is_open",
            "music",
            "list_music",
            "username",
        )


class AlbumSerializer(serializers.ModelSerializer):
    albums = common_serializers.UserListSerializer(many=True, read_only=True)
    users = serializers.ListSerializer(child=serializers.UUIDField(), write_only=True)

    class Meta:
        model = models.Album
        fields = ("id", "title", "type_album", "preview_image", "user", "users", "albums")
        read_only_fields = ("user",)


class OtherPlaylistSerializer(serializers.ModelSerializer):
    playlist_title = serializers.CharField(source="playlist.title", read_only=True)
    username = serializers.CharField(source="user.username", read_only=True)
    playlist_image = serializers.CharField(source="playlist.avatar", read_only=True)

    class Meta:
        model = models.MyPlaylists
        fields = ("id", "user", "playlist", "playlist_title", "username", "playlist_image")
        read_only_fields = ("user",)


class DetailAlbumSerializer(serializers.ModelSerializer):
    album = MusicSerializer(many=True, read_only=True)
    users = common_serializers.UserListSerializer(many=True, read_only=True)

    class Meta:
        model = models.Album
        fields = ("id", "title",  "user", "users", "date", "preview_image", "type_album", "album")

