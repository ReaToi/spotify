from django.conf import settings
from django.utils import timezone
from backend_apps.common import models as common_models
from django.db import models
from django.core.validators import FileExtensionValidator

# Create your models here.


class Album(common_models.BaseModel):
    user = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name='albums')
    title = models.CharField(max_length=250)
    type_album = models.CharField(max_length=20)
    date = models.DateField(default=timezone.now)
    preview_image = models.ImageField(
        upload_to='albums/previews', null=True, blank=True
    )

    def __str__(self):
        return f"{self.title} --- {self.user} --- {self.type_album}"


class Music(common_models.BaseModel):
    album = models.ForeignKey(Album, on_delete=models.CASCADE, null=True, blank=True, related_name='album')
    user = models.ManyToManyField(settings.AUTH_USER_MODEL, related_name="user_songs")
    title = models.CharField(max_length=250)
    duration = models.DurationField()
    auditions = models.PositiveIntegerField(default=0)
    file = models.FileField(
        upload_to='musics',
        validators=[
            FileExtensionValidator(
                allowed_extensions=[
                    'mp3', 'wav', "flac", "aac", "ogg", "m4a", "aiff", "wma"
                ]
            )
        ],
        null=True,
        blank=True,
    )

    def __str__(self):
        return f"{self.album} --- {self.album} --- {self.title} --- {self.auditions}"


class FavoriteMusics(common_models.BaseModel):
    user = models.OneToOneField(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    music = models.ManyToManyField(Music, blank=True)

    def __str__(self):
        return f"{self.user}"


class Playlist(common_models.BaseModel):
    user = models.ForeignKey(
        settings.AUTH_USER_MODEL,
        on_delete=models.CASCADE,
        verbose_name="creator",
        related_name="playlists",
        blank=True,
    )
    music = models.ManyToManyField(Music, blank=True)
    title = models.CharField(max_length=250)
    description = models.TextField(null=True, blank=True)
    avatar = models.ImageField(upload_to='albums/avatars', null=True, blank=True)
    is_open = models.BooleanField(default=True)

    def __str__(self):
        return f"{self.user} --- {self.title}"


class MyPlaylists(common_models.BaseModel):
    user = models.ForeignKey(settings.AUTH_USER_MODEL, on_delete=models.CASCADE)
    playlist = models.ForeignKey(Playlist, on_delete=models.CASCADE, related_name="playlists", null=True, blank=True)

    def __str__(self):
        return f"{self.user} --- {self.playlist}"
