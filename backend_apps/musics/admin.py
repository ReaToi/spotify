from django.contrib import admin

from backend_apps.musics import models


# Register your models here.


class MusicTabularInline(admin.StackedInline):
    model = models.Music


@admin.register(models.Music)
class MusicAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "duration", "auditions")
    list_display_links = ("id", "title")
    search_fields = ("title",)
    list_filter = ("auditions", "duration")


@admin.register(models.Album)
class AlbumAdmin(admin.ModelAdmin):
    list_display = ("id", "title", "type_album", "date")
    list_display_links = ("id", "title", "type_album", "date")
    search_fields = ("title",)
    inlines = (MusicTabularInline,)


admin.site.register(models.FavoriteMusics)
admin.site.register(models.Playlist)
admin.site.register(models.MyPlaylists)
