from rest_framework import generics, permissions, response, status, viewsets, views, renderers
from backend_apps.musics import serializers, models
from backend_apps.common import permissions as custom_permitions


# Create your views here.


class MyFavoriteMusicsAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.ListMusicsSerializer
    permission_classes = [permissions.IsAdminUser]

    def get_queryset(self):
        queryset = models.FavoriteMusics.objects.filter(user=self.request.user)
        return queryset

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        try:
            user, favorite_music = models.FavoriteMusics.objects.get_or_create(user=request.user)
            music = serializer.validated_data['list_music']
            user.music.add(music)
            return response.Response(data={"user": request.user.id,
                                           "music": f"{serializer.validated_data["list_music"]}"},
                                     status=status.HTTP_201_CREATED)
        except Exception as e:
            return response.Response(data={f"error": f"{e}"}, status=status.HTTP_400_BAD_REQUEST)


class MyFavoriteMusicsDeleteAPIView(views.APIView):
    def delete(self, request, *args, **kwargs):
        favorite_music = models.FavoriteMusics.objects.get(user=request.user)
        music = models.Music.objects.get(id=kwargs['id'])
        favorite_music.music.remove(music)
        return response.Response(data={}, status=status.HTTP_204_NO_CONTENT)


class MyCreatedPlaylistsAPIView(viewsets.ModelViewSet):
    lookup_field = 'id'
    serializer_class = serializers.PlaylistSerializer

    def get_queryset(self):
        queryset = models.Playlist.objects.filter(user=self.request.user)
        return queryset

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)

    def update(self, request, *args, **kwargs):
        playlist = models.Playlist.objects.get(id=self.kwargs['id'])
        serializer = self.get_serializer(playlist, data=request.data, partial=True)
        serializer.is_valid(raise_exception=True)
        try:
            list_music = serializer.validated_data['list_music']
            if list_music:
                playlist.music.add(serializer.validated_data['list_music'])
        except KeyError:
            pass
        serializer.save()
        return response.Response(data=serializer.data, status=status.HTTP_200_OK)

    def destroy(self, request, *args, **kwargs):
        playlist = models.Playlist.objects.get(id=self.kwargs['id'])
        if bool(request.data):
            serializer = self.get_serializer(data=request.data, partial=True)
            serializer.is_valid(raise_exception=True)
            list_music = serializer.validated_data["list_music"]
            if list_music:
                music = models.Music.objects.get(id=list_music)
                playlist.music.remove(music)
                return response.Response(data={"data": "delete music"}, status=status.HTTP_204_NO_CONTENT)
        playlist.delete()
        return response.Response(data={}, status=status.HTTP_204_NO_CONTENT)


class MusicsAPIView(generics.CreateAPIView):
    serializer_class = serializers.MusicSerializer
    queryset = models.Music.objects.all()
    permission_classes = [custom_permitions.IsArtistPermission]

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        artist = serializer.validated_data.pop("artists")
        music = models.Music.objects.create(**serializer.validated_data, album_id=kwargs['album'])
        try:
            music.user.set(artist)
        except Exception as e:
            return response.Response(data={f"error": f"{e}"})
        return response.Response(self.get_serializer(music).data,
                                 status=status.HTTP_201_CREATED)


class AlbumsAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.AlbumSerializer
    # queryset = models.Album.objects.all()

    def get_queryset(self):
        return models.Album.objects.filter(user=self.request.user)

    def post(self, request, *args, **kwargs):
        serializer = self.get_serializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        user = serializer.validated_data.pop("users")
        album = models.Album.objects.create(**serializer.validated_data)
        album.user.set(user)
        data = dict(self.serializer_class(album).data)
        return response.Response(data={**data, "user": user}, status=status.HTTP_201_CREATED)


class SearchMusicAPIView(generics.ListAPIView):
    serializer_class = serializers.MusicSerializer

    def get_queryset(self):
        return models.Music.objects.filter(title__icontains=self.request.query_params.get('q'))


class SearchAlbumsAPIView(generics.ListAPIView):
    serializer_class = serializers.AlbumSerializer

    def get_queryset(self):
        return models.Album.objects.filter(title__icontains=self.request.query_params.get('q'))


class SearchPlaylistAPIView(generics.ListAPIView):
    serializer_class = serializers.PlaylistSerializer

    def get_queryset(self):
        return models.Playlist.objects.filter(title__icontains=self.request.query_params.get('q'), is_open=True)


class OtherPlaylistAPIView(generics.ListCreateAPIView):
    serializer_class = serializers.OtherPlaylistSerializer

    def get_queryset(self):
        return models.MyPlaylists.objects.filter(user=self.request.user)

    def perform_create(self, serializer):
        serializer.save(user=self.request.user)


class DetailPlaylistAPIView(generics.RetrieveAPIView):
    serializer_class = serializers.PlaylistSerializer
    lookup_field = "id"
    queryset = models.Playlist.objects.all()


class DetailAlbumAPIView(generics.RetrieveAPIView):
    queryset = models.Album.objects.all()
    serializer_class = serializers.DetailAlbumSerializer
    lookup_field = "id"
