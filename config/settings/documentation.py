from django.urls import path
from rest_framework.authentication import SessionAuthentication
from rest_framework.permissions import IsAuthenticated
from drf_yasg.views import get_schema_view
from drf_yasg import openapi

schema_view = get_schema_view(
    openapi.Info(
        title="Spotify",
        default_version="v1",
        description="Production Spotify Api",
        license=openapi.License(name="ReaToi License"),
    ),
    public=True,
    permission_classes=[IsAuthenticated],
    authentication_classes=[SessionAuthentication]
)

urlpatterns = [
    path("swagger/1/", schema_view.without_ui(cache_timeout=0), name="schema-json"),
    path(
        "swagger/",
        schema_view.with_ui("swagger", cache_timeout=0),
        name="schema-swagger-ui",
    ),
    path("redoc/", schema_view.with_ui("redoc", cache_timeout=0), name="schema-redoc"),
]
